import { CommuneList } from "@/components/shared/CommuneList";
import { useCommuneSearchByDepartment } from "@/services/communes/hook";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import Grid from "@mui/material/Grid";
import { Loader } from "@/components/shared/Loader";
import { FetchStatus } from "@/components/shared/FetchStatus";
import { CommuneSearchItem } from "@/services/communes/type";

export const SearchDepartment = () => {
  const {
    region,
    department,
    setRegion,
    setDepartment,
    regionSWR,
    departmentSWR,
    communeSWR,
    error,
    isLoading,
  } = useCommuneSearchByDepartment();
  return (
    <>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6}>
          <FormControl fullWidth>
            <InputLabel id="region">Région</InputLabel>
            <Select
              labelId="region"
              id="region"
              value={region}
              label="Région"
              onChange={(e) => {
                setRegion(e.target.value);
              }}
            >
              {(regionSWR.data || []).map((r) => (
                <MenuItem key={r.code} value={r.code}>
                  {r.nom}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12} md={6}>
          <FormControl fullWidth>
            <InputLabel id="department">
              {!region ? "Veuillez choisir une région" : "Département"}
            </InputLabel>
            <Select
              labelId="department"
              id="department"
              value={department}
              label="Département"
              onChange={(e) => setDepartment(e.target.value)}
            >
              {(departmentSWR.data || []).map((r) => (
                <MenuItem key={r.code} value={r.code}>
                  {r.nom}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
      </Grid>
      <FetchStatus<Array<CommuneSearchItem>>
        swrStatus={communeSWR}
        noData={{
          text: `Veuillez choisir ${!region ? "une Région" : "un Département"}`,
          component: null,
        }}
      >
        {(data) => <CommuneList data={data} />}
      </FetchStatus>
    </>
  );
};
export default SearchDepartment;
