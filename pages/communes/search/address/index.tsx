import { CommuneList } from "@/components/shared/CommuneList";
import { useCommuneSearchByAddress } from "@/services/communes/hook";
import SearchIcon from "@mui/icons-material/SearchOutlined";
import FormHelperText from "@mui/material/FormHelperText";
import Container from "@mui/material/Container";
import FormControl from "@mui/material/FormControl";
import InputAdornment from "@mui/material/InputAdornment";
import OutlinedInput from "@mui/material/OutlinedInput";
import { FetchStatus } from "@/components/shared/FetchStatus";
import { CommuneSearchItem } from "@/services/communes/type";

export const SearchAddress = () => {
  const { address, setAddress, ...swrConfig } = useCommuneSearchByAddress();

  return (
    <Container disableGutters maxWidth="md">
      <FormControl variant="outlined" fullWidth>
        <OutlinedInput
          onChange={(e) => setAddress(e.target.value)}
          placeholder="Tapez une adresse"
          size="medium"
          startAdornment={
            <InputAdornment position="start">
              <SearchIcon />
            </InputAdornment>
          }
        />
        <FormHelperText variant="outlined">Minimum 3 caractères</FormHelperText>
      </FormControl>
      <FetchStatus<Array<CommuneSearchItem>>
        swrStatus={swrConfig}
        noData={{
          text:
            address.length > 2
              ? "Pas de communes trouvées"
              : "Minimum 3 caractères pour lancer la recherche",
          component: null,
        }}
      >
        {(data) => <CommuneList data={data} />}
      </FetchStatus>
    </Container>
  );
};

export default SearchAddress;
