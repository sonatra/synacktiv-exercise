import { useCommuneByCode } from "@/services/communes/hook";
import { NextPage } from "next";
import { useRouter } from "next/router";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import { LeafletMapProps } from "@/components/shared/LeafletMap";
import dynamic from "next/dynamic";
import { FetchStatus } from "@/components/shared/FetchStatus";
import { CommuneDetailResponse } from "@/services/communes/response";

/**
 * Dynamically load Leaflet Map without ssr knowing that we have
 * leaflet issue which need the client window property
 */
const Map = dynamic<LeafletMapProps>(
  async () => (await import("../../components/shared/LeafletMap")).LeafletMap,
  { ssr: false }
);

export const CommuneDetail: NextPage = () => {
  const router = useRouter();
  const { data, ...communeSWR } = useCommuneByCode(router.query.code as string);
  return (
    <FetchStatus<CommuneDetailResponse> swrStatus={{ ...communeSWR, data }}>
      {(data) => (
        <>
          <Map
            style={{ height: 400, width: "100%" }}
            lat={data.mairie.coordinates[1]}
            lng={data.mairie.coordinates[0]}
          />
          <Card>
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                {data.nom}
              </Typography>
              <List>
                <ListItem disableGutters disablePadding>
                  <ListItemText
                    primary={`Longitude: ${data.mairie.coordinates[0]}, Latitude: ${data.mairie.coordinates[1]}, `}
                  />
                </ListItem>
                <ListItem disableGutters disablePadding>
                  <ListItemText
                    primary={`Population: ${data.population} habitants`}
                  />
                </ListItem>
                <ListItem disableGutters disablePadding>
                  <ListItemText primary={`Surface: ${data.surface} km2`} />
                </ListItem>
                <ListItem disableGutters disablePadding>
                  <ListItemText
                    primary={`Code postal: ${data.codesPostaux[0]}`}
                  />
                </ListItem>
                <ListItem disableGutters disablePadding>
                  <ListItemText primary={`Code département: ${data.code}`} />
                </ListItem>
                <ListItem disableGutters disablePadding>
                  <ListItemText primary={`Région: ${data.region.nom}`} />
                </ListItem>
                <ListItem disableGutters disablePadding>
                  <ListItemText primary={`Code Région: ${data.region.code}`} />
                </ListItem>
              </List>
            </CardContent>
          </Card>
        </>
      )}
    </FetchStatus>
  );
};

export default CommuneDetail;
