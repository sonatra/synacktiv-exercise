import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Head from "next/head";
import Link from "next/link";
import { CSSProperties } from "react";

export default function Home() {
  const menuButtonStyle: CSSProperties = { padding: 20, marginTop: 4 };
  return (
    <>
      <Head>
        <title>Synacktiv exercise</title>
        <meta name="description" content="Synacktive exercise" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Typography variant="h5">Recherche de communes</Typography>
      <Link href="/communes/search/address">
        <Button
          fullWidth
          variant="contained"
          color="info"
          style={menuButtonStyle}
        >
          <Typography variant="body1">Rechercher par adresse</Typography>
        </Button>
      </Link>
      <Link href="/communes/search/department">
        <Button
          fullWidth
          variant="contained"
          color="info"
          style={menuButtonStyle}
        >
          <Typography variant="body1">
            Rechercher par régions et départements
          </Typography>
        </Button>
      </Link>
    </>
  );
}
