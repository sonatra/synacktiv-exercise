export type CommuneSearchItem = {
  label?: string;
  postcode?: string;
  code: string;
  name: string;
  context: string;
};
