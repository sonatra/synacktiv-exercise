import { API_DATAGOUV_ADDRESS, API_DATAGOUV_GEO } from "../../utils/constant";

export const CommuneAPIQuery = {
  regions: `${API_DATAGOUV_GEO}/regions`,
  departmentsByRegionCode: (regionCode: string) =>
    `${API_DATAGOUV_GEO}/regions/${regionCode}/departements?fields=nom,code`,
  communesByAddress: (address: string) =>
    `${API_DATAGOUV_ADDRESS}/search?q=${address}&limit=50`,
  communesByDepartmentCode: (departmentCode: string) =>
    `${API_DATAGOUV_GEO}/departements/${departmentCode}/communes?fields=nom,population`,
  communeByCode: (communeCode: string) =>
    `${API_DATAGOUV_GEO}/communes/${communeCode}?fields=code,nom,surface,population,codesPostaux,departement,region,mairie`,
};
