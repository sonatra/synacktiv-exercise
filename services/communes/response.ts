export type CommuneSearchResponse = {
  type: string;
  version: string;
  features: Array<{
    geometry: { type: string; coordinates: [number, number] };
    properties: {
      city: string;
      citycode: string;
      context: string;
      label: string;
      name: string;
      postcode: string;
      score: number;
      street: string;
      x: number;
      y: number;
      // And many more properties
      [key: string]: unknown;
    };
  }>;
  // And many more properties
  [key: string]: unknown;
};

export type CommuneDepartmentResponse = Array<{
  nom: string;
  population: string;
  code: string;
}>;

export type RegionResponse = {
  nom: string;
  code: string;
};

export type DepartmentResponse = {
  nom: string;
  code: string;
};

export type CommuneDetailResponse = {
  code: string;
  nom: string;
  surface: string;
  population: string;
  codesPostaux: Array<string>;
  departement: DepartmentResponse;
  region: RegionResponse;
  mairie: { type: string; coordinates: [number, number] };
  [key: string]: unknown;
};
