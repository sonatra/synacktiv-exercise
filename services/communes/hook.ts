import debounce from "lodash/debounce";
import {
  CommuneDepartmentResponse,
  CommuneDetailResponse,
  CommuneSearchResponse,
  DepartmentResponse,
  RegionResponse,
} from "@/services/communes/response";
import { CommuneSearchItem } from "@/services/communes/type";
import { fetcher } from "@/utils/fetcher";
import { useCallback, useEffect, useMemo, useState } from "react";
import useSWR from "swr/immutable";
import { CommuneAPIQuery } from "./api-query";

/**
 * Hook for address commune search
 * Use the ready to use function setAddress to call gouv api endpoints by address.
 * Every setAddress call is debounce by 300ms.
 * We ensure that we will not have any commune duplicate when we get data by their citycode
 * Finally we convert the response into a `CommuneSearchItem` to use it in the Generic component @see CommuneList
 */
export const useCommuneSearchByAddress = () => {
  const [address, setAddress] = useState<string>("");
  const { data, error, isLoading } = useSWR<CommuneSearchResponse>(
    address ? CommuneAPIQuery.communesByAddress(address) : null,
    fetcher
  );

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const setDebounceAddress = useCallback(debounce(setAddress, 300), []);

  const communes: Array<CommuneSearchItem> = useMemo(
    () =>
      data
        ? Object.values(
          data.features.reduce<Record<string, CommuneSearchItem>>(
            (p, c) => ({
              ...p,
              [c.properties.citycode]: {
                label: p[c.properties.citycode]
                  ? `${(p[c.properties.citycode].label, c.properties.street)}`
                  : c.properties.street,
                name: c.properties.city,
                code: c.properties.citycode,
                postcode: c.properties.postcode,
                context: c.properties.context,
              },
            }),
            {}
          )
        )
        : [],
    [data]
  );

  return {
    data: communes,
    address,
    setAddress: (address: string) =>
      address.length > 2 && setDebounceAddress(address),
    isLoading,
    error,
  };
};

/**
 * Hook for finding communes based on their department
 * We must get the region data list first
 * As soon as the user select the department we fetch the commune list
 * Base on the user fill inputs, we try to reconstruct the context property to get a seamless result than the `CommuneSearchResponse`
 * Finally we convert the response into a `CommuneSearchItem` to use it in the Generic component @see CommuneList
 */
export const useCommuneSearchByDepartment = () => {
  const [department, setDepartment] = useState<string>("");
  const [region, setRegion] = useState<string>("");

  const regionSWR = useSWR<Array<RegionResponse>>(
    CommuneAPIQuery.regions,
    fetcher
  );
  const departmentSWR = useSWR<Array<DepartmentResponse>>(
    region ? CommuneAPIQuery.departmentsByRegionCode(region) : null,
    fetcher
  );

  const { mutate: mutateCommuneSWR, ...communeSWR } =
    useSWR<CommuneDepartmentResponse>(
      department ? CommuneAPIQuery.communesByDepartmentCode(department) : null,
      fetcher
    );

  const communes: Array<CommuneSearchItem> = useMemo(
    () =>
      communeSWR.data && department && region
        ? communeSWR.data.map((f) => {
          const departmentName = departmentSWR.data?.find(
            (d) => d.code === department
          )?.nom;
          const regionName = regionSWR.data?.find(
            (r) => r.code === region
          )?.nom;
          return {
            label: `${f.population} habitants`,
            code: f.code,
            name: f.nom,
            // Get the same context as `useCommuneSearchByAddress`
            context: `${department}, ${departmentName}, ${regionName}`,
          };
        })
        : [],
    [communeSWR.data, department, region, departmentSWR.data, regionSWR.data]
  );

  /* Every time 'Region' change, clear data & department */
  useEffect(() => {
    setDepartment("");
    mutateCommuneSWR([]);
  }, [mutateCommuneSWR, region]);

  return {
    region,
    department,
    setDepartment,
    setRegion,
    regionSWR,
    departmentSWR,
    communeSWR: { ...communeSWR, data: communes },
    isLoading:
      regionSWR.isLoading || departmentSWR.isLoading || communeSWR.isLoading,
    error: regionSWR.error || departmentSWR.error || communeSWR.error,
  };
};

/* Code can be empty */
export const useCommuneByCode = (code?: string) =>
  useSWR<CommuneDetailResponse>(
    code ? CommuneAPIQuery.communeByCode(code) : null,
    fetcher
  );
