import { SWRResponse } from "swr";

export enum FetchStatusType {
  Null = "Null",
  Loading = "Loading",
  Error = "Error",
  NoData = "NoData",
  Success = "Success",
}

export type SWRStatusType = Omit<SWRResponse, "isValidating" | "mutate">;
export const getFetchStatus = ({
  isLoading,
  error,
  data,
}: SWRStatusType): FetchStatusType => {
  if (isLoading) return FetchStatusType.Loading;
  if (!!error) return FetchStatusType.Error;
  if (data)
    if (Array.isArray(data) ? data.length == 0 : !data)
      return FetchStatusType.NoData;
    else return FetchStatusType.Success;
  return FetchStatusType.Null;
};
