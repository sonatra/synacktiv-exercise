/* Simple fetcher for swr @see useSWR librairy */
export const fetcher = (input: RequestInfo | URL, init?: RequestInit) =>
  fetch(input, init).then((res) => res.json());
