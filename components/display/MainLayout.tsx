import { MainHeader } from "@/components/display/MainHeader";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import { ReactNode } from "react";
import { useTheme } from "@mui/material/styles";

export type LayoutProps = {
  children: ReactNode;
};

export const MainLayout = ({ children }: LayoutProps) => {
  const theme = useTheme();
  return (
    <>
      <CssBaseline />
      <MainHeader />
      <Container
        disableGutters
        maxWidth="md"
        style={{ padding: theme.spacing(2) }}
      >
        {children}
      </Container>
    </>
  );
};
