import { AppBar, Toolbar, Typography } from "@mui/material";
export const MainHeader = () => {
  return (
    <AppBar position="static">
      <Toolbar>
        <Typography>Synacktiv exercise</Typography>
      </Toolbar>
    </AppBar>
  );
};
