import { MapContainer, MapContainerProps, TileLayer } from "react-leaflet";

/**
 * Openstreetmap with only one single marker
 */
export type LeafletMapProps = MapContainerProps & {
  lat: number;
  lng: number;
};
export const LeafletMap = ({ lat, lng, ...restProps }: LeafletMapProps) => (
  <MapContainer
    {...restProps}
    center={{
      lat,
      lng,
    }}
    zoom={13}
  >
    <TileLayer
      attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
    />
  </MapContainer>
);
