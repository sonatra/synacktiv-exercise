import { CircularProgress, Typography } from "@mui/material";

export const Loader = () => (
  <div
    style={{
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      flexDirection: "column",
      minHeight: 200,
      gap: 20,
    }}
  >
    <CircularProgress disableShrink size={40} />
    <Typography>Recherche en cours ...</Typography>
  </div>
);
