import { CommuneSearchItem } from "@/services/communes/type";
import Avatar from "@mui/material/Avatar";
import List from "@mui/material/List";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import ListItemText from "@mui/material/ListItemText";
import CommuneIcon from "@mui/icons-material/GpsFixedTwoTone";
import LaunchOutlinedIcon from "@mui/icons-material/LaunchOutlined";
import ListItem from "@mui/material/ListItem";
import { Typography } from "@mui/material";
import { useRouter } from "next/router";
export type CommuneListProps = {
  data: Array<CommuneSearchItem>;
};
export const CommuneList = ({ data }: CommuneListProps) => {
  const router = useRouter();
  return (
    <List sx={{ width: "100%" }}>
      {data.map((d, i) => (
        <ListItem
          key={`${d.code}_${i}`}
          secondaryAction={<LaunchOutlinedIcon />}
          disablePadding
        >
          <ListItemButton onClick={() => router.push(`/communes/${d.code}`)}>
            <ListItemAvatar>
              <Avatar>
                <CommuneIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText
              primary={
                <>
                  <span>
                    {d.postcode} {d.name}{" "}
                  </span>
                  {d.label && (
                    <Typography variant="caption" fontStyle="italic">
                      - {d.label}
                    </Typography>
                  )}
                </>
              }
              secondary={
                <>
                  {d.context} - <i>Code INSEE {d.code}</i>
                </>
              }
            />
          </ListItemButton>
        </ListItem>
      ))}
    </List>
  );
};
