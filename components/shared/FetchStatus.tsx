import CircularProgress from "@mui/material/CircularProgress";
import Typography from "@mui/material/Typography";
import { ReactNode, useMemo } from "react";
import ErrorIcon from "@mui/icons-material/ReportOutlined";
import DataIcon from "@mui/icons-material/SimCardAlertOutlined";
import {
  FetchStatusType,
  SWRStatusType,
  getFetchStatus,
} from "@/utils/fetch-status";

export type FetchStatusProps<T> = {
  /* Pass down swr status response ( error, isLoading, data ) */
  swrStatus: SWRStatusType;
  /* Custom no data visualisation */
  noData?: { text: string; component: ReactNode };
  children: (data: T) => ReactNode;
};

/**
 * A simple generic component to have a generic message and component
 * for any swr data fetching statement
 * We ensure that we have data when we passing down data to children
 */
export const FetchStatus = <T extends Record<string, any>>({
  swrStatus,
  noData,
  children,
}: FetchStatusProps<T>) => {
  const fetchStatus = useMemo(() => getFetchStatus(swrStatus), [swrStatus]);
  const statusResult = useMemo(
    () => ({
      [FetchStatusType.Loading]: {
        text: "Recherche en cours ...",
        component: <CircularProgress disableShrink size={40} />,
      },
      [FetchStatusType.Error]: {
        text: `Erreur durant la requête`,
        component: <ErrorIcon fontSize="large" />,
      },
      [FetchStatusType.NoData]: noData || {
        text: "Aucunes données trouvées",
        component: <DataIcon fontSize="large" />,
      },
    }),
    [noData]
  );
  if (fetchStatus === FetchStatusType.Null) return null;
  if (fetchStatus === FetchStatusType.Success)
    return <>{children(swrStatus.data)}</>;

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        minHeight: 200,
        gap: 20,
      }}
    >
      {statusResult[fetchStatus].component}
      <Typography variant="subtitle1">
        {statusResult[fetchStatus].text}
      </Typography>
    </div>
  );
};
